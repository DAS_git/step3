from lxml import html
import csv
import pandas as pd
from pandas import Grouper
import numpy as np
import datetime
import matplotlib.pyplot as plt
from matplotlib.dates import HourLocator, DateFormatter


with open('report_201907021220.xls', 'r', encoding='utf-8') as f:
    page = f.read()
    parser = html.fromstring(page)
    keys = [i.text_content() for i in list(parser.xpath("//thead/tr")[0])]
    orders = dict()
    for tr in parser.xpath("//tbody/tr"):
        if len(tr) == 8:
            driver_id = tr.get('data-guid')
        else:
            key = tr.get('data-guid')
            orders[key] = {k: v for k, v in zip (keys, [i.text_content() for i in list(tr)])}
            orders[key]['id водителя'] = driver_id


with open('file.csv', 'w', encoding='utf-8', newline='') as file:
    writer = csv.writer(file, delimiter=',')
    for k, v in orders.items():
        row = [k] + list(v.values())
        writer.writerow(row)


def cdf_pdf(x):
    plt.subplot(311)
    plt.title('функция распределения и плотность вероятности для сумм заказов')
    plt.hist(x, alpha=1, bins=100, cumulative=True, color='g')
    plt.grid(True)
    plt.subplot(312)
    plt.hist(x, alpha=0.7, bins=50, density=True, color='b')
    plt.grid(True)
    return plt.savefig('file.png')


def results(data):
    return print(data[data['Способ оплаты'] == 'Безналичные']['Способ оплаты'].count(),"- безналичные заказы", '\n',
                 data[data['Способ оплаты'] == 'Наличные']['Способ оплаты'].count(),"- наличные заказы", '\n',
                 data.groupby(['id водителя'])['Сумма'].agg(['sum']), '\n',
                 data.groupby(['id водителя'])['Комиссия с водителя'].agg(['sum'])
                 )


def number_of_orders_plot(data):
    data['Заказ'] = pd.to_datetime(data['Заказ'])
    data = data[['Заказ', 'id заказа']]
    data = data.sort_values(by='Заказ')
    data = data.groupby(pd.Grouper(key='Заказ', freq='300S')).count()
    ax = fig.add_subplot(313)
    ax.plot(data)
    ax.xaxis.set_major_locator(HourLocator())
    ax.xaxis.set_major_formatter(DateFormatter("%H:%M"))
    fig.autofmt_xdate()
    plt.yticks(np.arange(0, 30, 5))
    plt.xlabel('время (каждые 5 минут)')
    plt.ylabel('кол-во заказов')
    plt.grid(True)
    return plt.savefig('file.png')


with open('file.csv', 'r', encoding='utf-8', newline='') as file:
    df = pd.read_csv(file, delimiter=',', decimal=',',
                     names=['id заказа'] + keys + ['id водителя'])
    df = df[df['Сумма'] > 0]
    # выводим начальные статистические запросы
    results(df)
    fig = plt.figure(figsize=[10,10])
    # грофик количества заказов от времени
    number_of_orders_plot(df)
    # функция распределения и плотность вероятности
    cdf_pdf(df['Сумма'])
    results(df)
